#include <iostream> 
#include "calculator.h"

int main(int argc, char** argv)
{  
    calculator c;
    int result;

    // success flag
    int flag = 0;

    // test add() function
    result = c.add(5,3);
    if(result == 8) {
        std::cout << "[OK] TEST #1: calculator::add()" << std::endl;
    } else {
        std::cout << "[FAIL] TEST #1: calculator::add() returned " << result << std::endl;
        flag = -1;
    }

    // test subtract() function
    result = c.subtract(5,3);
    if(result == 2) {
        std::cout << "[OK] TEST #2: calculator::subtract()" << std::endl;
    } else {
        std::cout << "[FAIL] TEST #2: calculator::subtract() returned " << result << std::endl;
        flag = -1;
    }
        
    return flag;
}
