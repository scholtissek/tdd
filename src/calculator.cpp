#include <iostream>
#include "calculator.h"

calculator::calculator() {
    std::cout << "-- calculator activated" << std::endl;
}

calculator::~calculator() {
    std::cout << "-- calculator de-activated" << std::endl;
}

int calculator::add(int a, int b) {
    return a+b;
}

int calculator::subtract(int a, int b) {
    return a-b;
}

void info(){
    printf("This is the info function!\n");
}
